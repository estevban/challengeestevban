import React from 'react';
import './transfer-list.css';
import DynamicTable from '../../../../tools/dynamic-table/dynamic-table';
import { GeneralLargeDateFromUnixTime, GeneralCurrency } from '../../../../tools/helpers/transform';
import { PaymentsDB } from '../../../../tools/fake-db/payments-db';

class TransferList extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            data: PaymentsDB(props).data.payments
            , columnDef: [
                {
                    columnId: 'customerName'
                    , columnName: 'Name'
                    , cell: (element) => {
                        return (
                            <p>
                                {element.customer.name}
                            </p>
                        )
                    }
                }
                ,
                {
                    columnId: 'customerEmail'
                    , columnName: 'Email'
                    , cell: (element) => {
                        return (
                            <p>
                                {element.customer.email}
                            </p>
                        )
                    }
                }
                ,
                {
                    columnId: 'created'
                    , columnName: 'date'
                    , cell: (element) => {
                        return (
                            <p>
                                {GeneralLargeDateFromUnixTime({ date: element.created })}
                            </p>
                        )
                    }
                }
                ,
                {
                    columnId: 'amount'
                    , columnName: 'Amount'
                    , cell: (element) => {
                        return (
                            <p>{
                                GeneralCurrency({ amount: element.amount })
                            }</p>
                        )
                    }
                }                
            ]
        }
    }

    render() {
        return (
            <React.Fragment>
                <h1 className="content-title">Transfers</h1>
                <div className="detail-list">
                    <DynamicTable dataset={this.state.data} columnDef={this.state.columnDef} />
                </div>
            </React.Fragment>
        );
    }
}

export default TransferList;