import React, { PureComponent } from 'react';
import './chargeback-list.css';
import { PaymentsDB } from '../../../../tools/fake-db/payments-db';
import DynamicTable from '../../../../tools/dynamic-table/dynamic-table';

class ChargeBackList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: PaymentsDB(props).data.payments
            , columnDef: [
                {
                    columnId: 'id'
                    , columnName: 'Id'
                }
                ,
                {
                    columnId: 'status'
                    , columnName: 'Status'
                }
                ,
                {
                    columnId: 'failure'
                    , columnName: 'Failure'
                }
                ,
                {
                    columnId: 'type'
                    , columnName: 'Type'
                }
                ,
                {
                    columnId: 'brand'
                    , columnName: 'Brand'
                }
            ]
        }
    }
    render() {
        return (
            <React.Fragment>
                <h1 className="content-title">Chargebacks</h1>
                <div className="detail-list">
                    <DynamicTable dataset={this.state.data} columnDef={this.state.columnDef} />
                </div>
            </React.Fragment>
        );
    }
}

export default ChargeBackList;