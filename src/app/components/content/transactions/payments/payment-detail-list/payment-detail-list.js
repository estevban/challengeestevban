import React, { PureComponent } from 'react';
import { FaExclamation, FaAdversal, FaCheck, FaArrowLeft } from 'react-icons/fa';
import './payment-detail-list.css';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';
import Card from '../../../../tools/card/card';
import SmartLabel from '../../../../tools/smart-label/smart-label';
import { IconFinder } from '../../../../tools/icon-finder/icon-finder';
import { PaymentsDB, PaymentDetailsDb } from '../../../../tools/fake-db/payments-db';
import { GeneralCurrency, GeneralDateFromUnixTime, GeneralLargeDateFromUnixTime } from '../../../../tools/helpers/transform';
import DynamicTable from '../../../../tools/dynamic-table/dynamic-table';
import { ExternalImage } from '../../../../tools/helpers/external-image';
import ImageIcon from '../../../../tools/image-icon/image-icon';

class PaymentDetailList extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            id: atob(this.props.match.params.id)
            , detail: PaymentDetailsDb({ id: atob(this.props.match.params.id) }).data.payment
            , breackdownColumnDef: [
                {
                    columnId: 'sku'
                    , columnName: 'SKU'
                }
                ,
                {
                    columnId: 'quantity'
                    , columnName: 'Quantity'
                }
                ,
                {
                    columnId: 'name'
                    , columnName: 'Articles'
                }
                ,
                {
                    columnId: 'taxes'
                    , columnName: 'Taxes'
                }
                ,
                {
                    columnId: 'discounts'
                    , columnName: 'Discounts'
                }
                ,
                {
                    columnId: 'unit_price'
                    , columnName: 'Unit Price'
                    , cell: (element) => {
                        return <p>
                            {GeneralCurrency({ amount: element.unit_price })}
                        </p>
                    }
                }
            ]
            , attempsColumnDef: [
                {
                    columnId: 'created'
                    , columnName: 'Date'
                    , cell: (element) => {
                        return <p>
                            {GeneralDateFromUnixTime({ date: element.created })}
                        </p>
                    }
                }
                ,
                {
                    columnId: 'status'
                    , columnName: 'Status'
                    , align: 'center'
                    , style: (element) => {
                        let style = {};
                        switch (element.status) {
                            case 'declined':
                                style = {
                                    backgroundColor: '#d9adad', borderRadius: '23px'
                                }
                                break;
                            case 'partially_refunded':
                                style = {
                                    backgroundColor: '#add1d9', borderRadius: '23px'
                                }
                                break;
                            case 'paid':
                                style = {
                                    backgroundColor: '#3c9c4e', borderRadius: '23px'
                                }
                                break;
                        }
                        return style;
                    }
                    , icon: (element) => {
                        switch (element.status) {
                            case 'declined':
                                return <FaExclamation />
                                break;
                            case 'partially_refunded':
                                return <FaAdversal />
                                break;
                            case 'paid':
                                return < FaCheck />
                            default:
                                return <p></p>
                                break;
                        }
                    }
                }
                ,
                {
                    columnId: 'payment_method'
                    , columnName: 'Payment method'
                    , align: 'center'
                    , cell: (element) => {
                        return <React.Fragment>
                            <p>{element.payment_method.type}</p>
                            <ImageIcon uri={ExternalImage({ name: element.payment_method.brand })} />
                        </React.Fragment>


                    }
                }
                , {
                    columnId: 'amount'
                    , columnName: 'Amount'
                    , align: 'left'
                    , style: (element) => {
                        return {
                            fontWeight: 'bold',
                            fontSize: '15px'
                        }

                    }
                    , cell: (element) => {
                        return <p>
                            {GeneralCurrency({ amount: element.amount })}
                        </p>
                    }
                }
            ]
        }
        this.handleBack = this.handleBack.bind(this);
    }

    componentDidMount() {
        console.log(this.state.detail);
    }

    handleBack() {
        this.props.history.push('/payment-list');
    }

    render() {
        console.log(this.state.detail.nested_charges.length);
        let lastNested = this.state.detail.nested_charges[this.state.detail.nested_charges.length - 1];
        let nestedCharges = this.state.detail.nested_charges;
        let lineItems = this.state.detail.line_items;
        let total = lineItems.map(c => c.unit_price).reduce((p, c) => p + c, 0);
        let fees = total * 0.16;
        let subtotal = total - fees;

        return (
            <div>
                <div className="header-detail">
                    <FaArrowLeft onClick={this.handleBack} className="header-icon" size="25px" />
                    <h1>
                        Payment Detail
                </h1>
                </div>
                <div>
                    <Tabs>
                        <TabList>
                            <Tab>Data</Tab>
                            <Tab>Breakdown</Tab>
                        </TabList>
                        <TabPanel>
                            <div className="detail-card-group">
                                <Card alterClass="payment" title="Payment Status">
                                    <div className="tab-payment">
                                        <SmartLabel customClassName="amount" title="Amount" text={GeneralCurrency({ amount: this.state.detail.amount })} textSize="2em" />
                                        <div className="tab-payment-group-created">
                                            <SmartLabel customClassName="create-at" title="Created At" text={GeneralLargeDateFromUnixTime({ date: this.state.detail.created })} flow="column" />
                                            <SmartLabel customClassName="paid-at" title="Paid At" titleColor="#c8c8c8" textColor='#c8c8c8' text={GeneralLargeDateFromUnixTime({ date: this.state.detail.paid_at })} flow="column" />
                                        </div>
                                        <SmartLabel icon={IconFinder({ iconName: 'FaExclamation' })} customClassName="declined" titleBackground="#eb8e8e" titleColor="#f00b0b" title={this.state.detail.status} text={this.state.detail.description} flow="column" />
                                        <SmartLabel icon={IconFinder({ iconName: 'FaExternalLinkAlt' })} customClassName="order-id" title="Order ID" text={this.state.detail.id} />
                                        <div className="tab-payment-group-view">
                                            <SmartLabel icon={IconFinder({ iconName: 'FaLink' })} customClassName="view-transfer" title="View Transfer" text="" flow="column" />
                                            <SmartLabel icon={IconFinder({ iconName: 'FaLink' })} customClassName="view-chargeback" title="View Chargeback" titleColor="#c8c8c8" textColor='#c8c8c8' text="" flow="column" />
                                        </div>
                                    </div>
                                </Card>
                                <Card alterClass="client" title="Client">
                                    <SmartLabel icon={IconFinder({ iconName: 'FaUserAlt' })} customClassName="name" title="Name" text={this.state.detail.customer.name} />
                                    <SmartLabel icon={IconFinder({ iconName: 'FaMailBulk' })} customClassName="email" title="Email" text={this.state.detail.customer.email} />
                                    <SmartLabel icon={IconFinder({ iconName: 'FaPhone' })} customClassName="phone-number" title="Phone number" text={this.state.detail.customer.phone} />

                                </Card>
                                <Card alterClass="method" title="Payment Method">
                                    <div className="tab-method">
                                        <SmartLabel icon={IconFinder({ iconName: 'FaCcVisa' })} title={lastNested.payment_method.bank} text={lastNested.payment_method.type} flow="column" />
                                        <div className="tab-method-group-card">
                                            <SmartLabel customClassName="card-number" center="true" title={("**** **** **** " + lastNested.payment_method.last4)} titleColor='#fff' textColor='#fff' text={lastNested.payment_method.name} />
                                            <SmartLabel customClassName="card-expiration" title="Expiration Date" titleColor='#fff' titleSize="14.5px" textColor='#fff' text={(`${lastNested.payment_method.exp_month}/${lastNested.payment_method.exp_year}`)} flow="column" />
                                            <SmartLabel customClassName="card-code" title="Authorization Code" titleColor='#fff' titleSize="14.5px" textColor='#fff' text={lastNested.payment_method.auth_code} flow="column" />
                                        </div>
                                    </div>

                                </Card>
                                <div className="attemps">
                                    <Tabs>
                                        <TabList>
                                            <Tab>Payment attemps</Tab>
                                            <Tab>Refunds</Tab>
                                            <Tab>Notifications</Tab>
                                        </TabList>
                                        <TabPanel>
                                            <DynamicTable dataset={nestedCharges} columnDef={this.state.attempsColumnDef} />
                                        </TabPanel>
                                        <TabPanel>
                                            <h2>Refunds</h2>
                                        </TabPanel>
                                        <TabPanel>
                                            <h2>Notifications</h2>
                                        </TabPanel>
                                    </Tabs>
                                </div>
                            </div>
                        </TabPanel>
                        <TabPanel>
                            <div className="tab-breakdown">
                                <DynamicTable dataset={lineItems} columnDef={this.state.breackdownColumnDef} />
                                <div className="tab-breakdown-total">
                                    <SmartLabel titleWeight="normal" right="true" titleColor="#212529" title="Subtotal" text={GeneralCurrency({ amount: subtotal })} flow="column" />
                                    <SmartLabel titleWeight="normal"  right="true" titleColor="#212529" title="Fee" text={GeneralCurrency({ amount: fees })} flow="column" />
                                    <SmartLabel title="Total"  right="true"  titleColor="#212529" text={GeneralCurrency({ amount: total })} textWeight="bold" flow="column" />
                                </div>
                            </div>
                        </TabPanel>
                    </Tabs>
                </div>
            </div>

        );
    }
}

export default PaymentDetailList;
