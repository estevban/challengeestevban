import React from 'react';
import { PaymentsDB } from '../../../../tools/fake-db/payments-db';
import DynamicTable from '../../../../tools/dynamic-table/dynamic-table';
import { GeneralDate, GeneralCurrency, GeneralDateFromUnixTime } from '../../../../tools/helpers/transform';
import { FaRegEye, FaCheck, FaExclamation, FaAdversal } from 'react-icons/fa';
import { ExternalImage } from '../../../../tools/helpers/external-image';
import ImageIcon from '../../../../tools/image-icon/image-icon';
import './payment-list.css';

class PaymentList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            payments: PaymentsDB(props).data.payments
            , columnDef: [
                {
                    columnId: 'operations'
                    , columnName: 'Acciones'
                    , align: 'center'
                    , operations: [
                        {
                            icon: FaRegEye
                            , handleClick: (element) => {
                                this.props.history.push(`/payment-detail-list/${encodeURI(btoa(element.id))}`);
                            }
                        },
                        {
                            icon: FaCheck
                            , handleClick: (element) => {
                                console.log(element);
                            }
                        }
                    ]
                },
                {
                    columnId: 'created'
                    , columnName: 'Date'
                    , cell: (element) => {
                        return <p>
                            {GeneralDateFromUnixTime({ date: element.created })}
                        </p>
                    }
                }
                ,
                {
                    columnId: 'status'
                    , columnName: 'Payment status'
                    , align: 'center'
                    , style: (element) => {
                        let style = {};
                        switch (element.status) {
                            case 'declined':
                                style = {
                                    backgroundColor: '#d9adad', borderRadius: '23px'
                                }
                                break;
                            case 'partially_refunded':
                                style = {
                                    backgroundColor: '#add1d9', borderRadius: '23px'
                                }
                                break;
                            case 'paid':
                                style = {
                                    backgroundColor: '#3c9c4e', borderRadius: '23px'
                                }
                                break;
                        }
                        return style;
                    }
                    , icon: (element) => {
                        switch (element.status) {
                            case 'declined':
                                return <FaExclamation />
                                break;
                            case 'partially_refunded':
                                return <FaAdversal />
                                break;
                            case 'paid':
                                return <FaCheck />
                            default:
                                return <p></p>
                                break;
                        }
                    }
                }
                ,
                {
                    columnId: 'type'
                    , columnName: 'Payment type'
                    , align: 'center'
                    , cell: (element) => {
                        return <React.Fragment>
                            <p>{element.type}</p>
                            <ImageIcon uri={ExternalImage({ name: element.brand })} />
                        </React.Fragment>


                    }
                },
                {
                    columnId: 'customer'
                    , columnName: 'Customer'
                    , cell: (element) => {
                        return <div>
                            <h5> {element.customer.name}</h5>
                            <p> {element.customer.email}</p>
                        </div>

                    }
                }
                , {
                    columnId: 'amount'
                    , columnName: 'Amount'
                    , align: 'left'
                    , style: (element) => {
                        return {
                            fontWeight: 'bold',
                            fontSize: '15px'
                        }

                    }
                    , cell: (element) => {
                        return <p>
                            {GeneralCurrency({ amount: element.amount })}
                        </p>
                    }
                }
            ]
        };   
        
    }
    render() {
        console.log(this.state.payments);
        return (
            <div>
                <h1 className="content-title">Payments</h1>
                <div className="detail-list">
                    <DynamicTable dataset={this.state.payments} columnDef={this.state.columnDef} />
                </div>
            </div>
        );
    }
}

export default PaymentList;