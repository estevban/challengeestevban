import React from 'react';
import Card from '../../../tools/card/card';
import './home.css';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import SmartLabel from '../../../tools/smart-label/smart-label';
import { IconFinder } from '../../../tools/icon-finder/icon-finder';

class Home extends React.Component {
    render() {
        return (
            <React.Fragment>
                <h1>Home Esteban's Challenge</h1>
                <br></br>
                <div className="summary">
                    <h4>Components Development</h4>
                    <div >
                        <SmartLabel icon={IconFinder({ iconName: 'FaTextHeight' })} title="SmartLabel" text="To give a text with custom label, text, label size, text size, label color, text color, icon, label background, text background, etc." />
                        <SmartLabel icon={IconFinder({ iconName: 'FaTable' })} title="DynamicTable" text="To show a table that receive datasource and columns def" />
                        <SmartLabel icon={IconFinder({ iconName: 'FaSitemap' })} title="MenuItem" text="To show a menu item with icon and route " />
                        <SmartLabel icon={IconFinder({ iconName: 'FaSortAmountDown' })} title="MenuList" text="To show a set if menu items with icon and route " />
                        <SmartLabel icon={IconFinder({ iconName: 'FaRegIdCard' })} title="Card" text="Use to draw a card with header that can contain inner elements" />
                    </div>
                    <br></br>
                    <h4>Helpers Development</h4>
                    <SmartLabel titleColor='#00783d' icon={IconFinder({ iconName: 'FaRegGrinWink' })} title="IconFinder" text="To looks for a icon in the fa repo" flow="column" />
                    <SmartLabel titleColor='#a10000' icon={IconFinder({ iconName: 'FaRegImage' })} title="ExternalImage" text="To convert a text into url Image" flow="column" />
                    <SmartLabel titleColor='#0f007c' icon={IconFinder({ iconName: 'FaRegLightbulb' })} title="Helpers" text="To give correct date and currency formats" flow="column" />
                </div>
            </React.Fragment>

        );
    }
}

export default Home;