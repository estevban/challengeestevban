import React, { Component } from 'react';
import './header.css'
import { FaBars, FaCaretDown } from 'react-icons/fa';
import MenuItem from '../../tools/menu-item/menu-item';
import { IconFinder } from '../../tools/icon-finder/icon-finder';


class Header extends Component {
   
    render() {
        return (
            <div className="header ">
                <FaBars onClick={this.props.onFaBarsClick} className="header-menu-icon"></FaBars>
                <div className="header-user">
                    <img className="header-user-picture" src="https://img.icons8.com/plasticine/100/000000/user.png" />
                    <div className="header-user-data">
                        <p>User name</p>
                        <h6>Esteban Perez</h6>
                    </div>
                    <div className="header-user-icon" >
                        <FaCaretDown />
                        <div className="header-dropdown-content">
                            <MenuItem name="Me" url="/" menuIcon={IconFinder({ iconName: 'FaRegUserCircle' })} />
                            <MenuItem name="Logout" url="/logout" menuIcon={IconFinder({ iconName: 'FaRegWindowClose' })} />
                        </div>

                    </div>


                </div>
            </div>
        );
    }
}

export default Header;