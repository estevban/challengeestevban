import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import './content.css'
import PaymentList from '../../content/transactions/payments/payment-list/payment-list';
import Home from '../../content/index/home/home';
import TransferList from '../../content/transactions/transfers/transfer-list/transfer-list';
import ChargeBackList from '../../content/transactions/chargebacks/chargeback-list/chargeback-list';
import PaymentDetailList from '../../content/transactions/payments/payment-detail-list/payment-detail-list';

class Content extends Component {
    render() {
        return (
            <div className="content">
                <Route exact path="/" component={Home} />
                <Route path="/payment-list" component={PaymentList} />
                <Route path="/transfer-list" component={TransferList} />
                <Route path="/chargeback-list" component={ChargeBackList} />
                <Route path="/payment-detail-list/:id" component={PaymentDetailList} />
            </div>);
    }
}

export default Content;