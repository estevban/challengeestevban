import React from 'react';
import './sidebar.css';
import { FaBars } from "react-icons/fa";
import MenuList from '../../tools/menu-list/menu-list';
import { Link } from 'react-router-dom'

class Sidebar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            items:
                [
                    {
                        id: 1
                        , name: 'Home'
                        , url: '/'
                        , menuIcon: 'FaArtstation'
                    }
                    ,
                    {
                        id: 2
                        , name: 'Transactions'
                        , url: '#'
                        , menuIcon: 'FaBluetoothB'
                        , items: [
                            {
                                id: 3
                                , name: 'Payments'
                                , url: '/payment-list'
                                , menuIcon: 'FaCentos'
                            }
                            ,
                            {
                                id: 4
                                , name: 'Transfers'
                                , url: '/transfer-list'
                                , menuIcon: 'FaDropbox'
                            }
                            ,
                            {
                                id: 5
                                , name: 'Chargebacks'
                                , url: '/chargeback-list'
                                , menuIcon: 'FaFedora'
                            }
                        ]
                    },
                    {
                        id: 6
                        , name: 'Accounting'
                        , url: '#'
                        , menuIcon: 'FaFirefox'
                        , items: [
                            {
                                id: 3
                                , name: 'Pending'
                                , url: '/account-list'
                                , menuIcon: 'FaCentos'
                            }
                            ,
                            {
                                id: 4
                                , name: 'Defeat'
                                , url: '/account-payment-list'
                                , menuIcon: 'FaHornbill'
                            }
                            ,
                            {
                                id: 5
                                , name: 'Charges'
                                , url: '/account-charge-list'
                                , menuIcon: 'FaLastfm'
                            }
                        ]
                    }
                ]
        }
            ;
    }

    render() {
        return (
            <div className="sidebar">
                <div className="sidebar-title">
                    <h3>
                        <Link to="/">CHALLENGE</Link></h3>
                </div>
                <div className="sidebar-menu">
                    <MenuList items={this.state.items} />
                </div>
            </div>
        );
    }
}

export default Sidebar;