import React, { PureComponent } from 'react';
import './card.css'

class Card extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            title: props.title,
            alterClass: props.alterClass
        }
    }
    render() {
        return (
            <div className={["card", this.state.alterClass ? this.state.alterClass : ''].join(" ")}  >
                <div className="card-body">
                    <h5 className="card-title">{this.state.title}</h5>
                    <div className="card-text">
                        {this.props.children}
                        </div>

                </div>
            </div>
        );
    }
}

export default Card