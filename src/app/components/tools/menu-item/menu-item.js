import * as icons from 'react-icons/fa';
import React from 'react';
import './menu-item.css';
import IconFinder from '../icon-finder/icon-finder'
import {Link} from 'react-router-dom';

class MenuItem extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {

        return (
            <div className={this.props.sideClass ? this.props.sideClass : 'menu-item'} >
                <this.props.menuIcon />                
                <Link to={this.props.url}>{this.props.name}</Link>                
            </div>
        );
    }
}

export default MenuItem
