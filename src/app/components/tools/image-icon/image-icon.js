import React, { PureComponent } from 'react';
import './image-icon.css'
class ImageIcon extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            uri: props.uri
        }
    }
    render() {
        if (this.state.uri && this.state.uri.length) {
            return (
                <img src={this.state.uri}>
                </img>
            )
        }
        else
            return (
                <div></div>
            );
    }
}

export default ImageIcon;
