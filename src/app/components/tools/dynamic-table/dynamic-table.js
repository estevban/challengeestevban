import React, { PureComponent } from 'react';
import './dynamic-table.css'
class DynamicTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: props.dataset
            , columnDef: props.columnDef
        }        
    }
    render() {
        return (
            <div className="table-responsive">
                <table className="table table-hover">
                    <thead>
                        <tr>
                            {this.state.columnDef.map(column => {
                                return <th key={column.columnId} >{column.columnName}</th>
                            })}
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.data.map((row, index) => {
                            return (
                                <tr key={index}>
                                    {this.state.columnDef.map((column, indexColumn) => {
                                        if (!column.operations) {
                                            if (!column.cell) {
                                                return <td key={indexColumn} >
                                                    <div className={column.align ? column.align + '-row' : ''} style={column.style ? column.style(row) : {}}>
                                                        {row[column.columnId]}
                                                        {column.icon ? column.icon(row) : ''}
                                                    </div>
                                                </td>
                                            }
                                            else {
                                                return <td key={indexColumn}>
                                                    <div className={column.align ? column.align + '-row' : ''} style={column.style ? column.style(row) : {}}>
                                                        {column.cell(row)}
                                                        {column.icon ? column.icon(row) : ''}
                                                    </div>
                                                </td>
                                            }
                                        }
                                        else {
                                            return <td key={indexColumn} className={column.align ? column.align + '-row' : ''} style={column.style ? column.style(row) : {}}  >
                                                {column.operations.map((operation, indexOperation) => {
                                                    return <div key={indexOperation} >
                                                        <operation.icon onClick={() => operation.handleClick(row)} className="operation"  ></operation.icon>

                                                    </div>
                                                })}
                                            </td>
                                        }


                                    })}
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div >
        );
    }


}

export default DynamicTable;