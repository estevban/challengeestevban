import React from 'react';
import MenuItem from '../menu-item/menu-item';
import { IconFinder } from '../icon-finder/icon-finder.js'
import './menu-list.css'
class MenuList extends React.Component {
    constructor(props) {
        super(props);
        this.state = { items: props.items };
    }


    render() {
        return (
            <div>
                {this.getMenu(this.state.items)}
            </div>
        )
    }

    getMenu(items) {        
        return items.map(item => {
            if (item.url !== '#') {
                return (
                    <div key={item.id} className={this.props.childClass ? this.props.childClass : 'parent-item'}>
                        <MenuItem sideClass="side-item" key={item.id} name={item.name} url={item.url} menuIcon={IconFinder({ iconName: item.menuIcon })} />
                    </div>
                )
            }
            else {
                return (
                    <div key={item.id} className={this.props.childClass ? this.props.childClass : 'parent-item'}>
                        <MenuItem sideClass="side-item" key={item.id} name={item.name} url={item.url} menuIcon={IconFinder({ iconName: item.menuIcon })} />
                        <MenuList childClass="child-list" items={item.items} />
                    </div>
                )
            }
        });
    }
}

export default MenuList;