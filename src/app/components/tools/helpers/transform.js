import React, { PureComponent } from 'react';

export const GeneralDate = (props) => {
    return new Intl.DateTimeFormat('en-US', {
        year: 'numeric',
        month: 'long',
        day: '2-digit', hour: 'numeric', minute: 'numeric'
    }).format(props.date)
}

export const GeneralDateFromUnixTime = (props) => {
    if (props.date) {
        let date = new Date(props.date * 1000);
        return new Intl.DateTimeFormat('en-US', {
            year: 'numeric',
            month: 'long',
            day: '2-digit', hour: 'numeric', minute: 'numeric'
        }).format(date);
    }
    else {
        return '--';
    }
}
export const GeneralLargeDateFromUnixTime = (props) => {
    if (props.date) {
        let date = new Date(props.date * 1000);
        return new Intl.DateTimeFormat('en-US', {
            year: 'numeric',
            month: '2-digit',
            day: '2-digit', hour: 'numeric', minute: 'numeric'
        }).format(date);
    }
    else {
        return '--';
    }
}

export const GeneralCurrency = (props) => {
    return new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'MXN',
        minimumFractionDigits: 0,
        maximumFractionDigits: 0
    }).format(props.amount)
}
