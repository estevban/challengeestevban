export const ExternalImage = (props) => {
    let uri = "";
    switch (props.name) {
        case "visa":
            uri = "https://upload.wikimedia.org/wikipedia/commons/1/16/Former_Visa_%28company%29_logo.svg"
            break;
        case "american_express":
            uri = "https://www.americanexpress.com/content/dam/amex/za/network/images/card-image-theplatinumcard-454x283-transparent-background.png"
            break;
        case "mastercard":
            uri = "https://upload.wikimedia.org/wikipedia/commons/1/16/Former_Visa_%28company%29_logo.svg"
            break;
    }
    return uri;
}