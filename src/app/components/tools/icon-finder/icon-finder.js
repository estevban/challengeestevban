import * as icons from 'react-icons/fa';

export const IconFinder = (props) => {    
    return icons[props.iconName];    
}


