import React, { PureComponent } from 'react';
import './smart-label.css';
import { FaBars } from 'react-icons/fa';
import { IconFinder } from '../icon-finder/icon-finder';

class SmartLabel extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            title: props.title
            , text: props.text
            , config: props.config
            , titleSize: props.titleSize ? props.titleSize : '1em'
            , textSize: props.textSize ? props.textSize : '14.5px'
            , textWeight: props.textWeight ? props.textWeight : 'normal'
            , titleWeight: props.titleWeight ? props.titleWeight : 'bold'
            , titleColor: props.titleColor ? props.titleColor : '#7f7f7f'
            , textColor: props.textColor ? props.textColor : '#000'
            , flow: props.flow ? props.flow : 'row'
            , customClassName: props.customClassName ? props.customClassName : ''
            , titleBackground: props.titleBackground ? props.titleBackground : ''
            , icon: props.icon ? props.icon : ''
            , center: props.center ? props.center : false
            , right: props.right ? props.right : false
        }

        this.getIcon = this.getIcon.bind(this);

    }
    render() {



        let titleStyle = {
            fontSize: this.state.titleSize,
            color: this.state.titleColor,
            fontWeight: this.state.titleWeight,
            background: this.state.titleBackground
        };

        let textStyle = {
            fontSize: this.state.textSize,
            color: this.state.textColor,
            fontWeight: this.state.textWeight
        };

        return (
            <div className={[("smart-label"), this.state.customClassName].join(' ')}>
                <div className="smart-label-icon">
                    {this.getIcon(this.state)}
                </div>
                <div className={("smart-label-content-" + this.state.flow + (this.state.center ? '-center' : '') + (this.state.right ? '-right' : ''))} >

                    <label style={titleStyle}>{this.state.title}</label>
                    <p style={textStyle}>{this.state.text}</p>
                </div>
            </div>

        );
    }

    getIcon(state) {
        if (state.icon != '')
            return (
                <state.icon />
            )
    }
}

export default SmartLabel;