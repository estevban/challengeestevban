import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Header from './app/components/layout/header/header';
import Content from './app/components/layout/content/content';
import Sidebar from './app/components/layout/sidebar/sidebar';
import 'bootstrap/dist/css/bootstrap.css';
import { BrowserRouter as Router } from 'react-router-dom';


class App extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            showSidebar: true,
            main: 'main-container visible',
            height: window.innerHeight,
            width: window.innerWidth
        };
        this.resize = this.resize.bind(this);
        this.handleFaBarsClick = this.handleFaBarsClick.bind(this);

    }

    resize() {
        let showSidebar = this.state.showSidebar;
        if (window.innerWidth < 768) {
            showSidebar = false;

        }
        this.setState({ showSidebar: showSidebar, width: window.innerWidth, height: window.innerHeight });
    }

    componentDidMount() {
        window.addEventListener('resize', this.resize)
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.resize)
    }

    handleFaBarsClick() {
        let showSidebar = !this.state.showSidebar;
        this.setState({ showSidebar: showSidebar });
    }

    render() {
        return (
            <div className={this.state.showSidebar ? 'main-container visible' : 'main-container hide'}  >
                <Router>
                    <Sidebar />
                    <Header onFaBarsClick={this.handleFaBarsClick} />
                    <Content />
                </Router>
            </div>
        );
    }
}


ReactDOM.render(<App />, document.getElementById('app'))
